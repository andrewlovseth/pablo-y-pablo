const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();

function errorlog(err){
    console.error(err.message);
    this.emit('end');
}

function style() {

    return gulp.src('sass/pabloypablo.scss')
        .pipe(sass().on('error', errorlog))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./'))
        .pipe(browserSync.stream());
}

function watch() {
    browserSync.init({
        proxy: "https://pabloypablo.local"
    });

    gulp.watch('./sass/**/*.scss', style);
    gulp.watch('./*.php').on('change', browserSync.reload);
    gulp.watch('./partials/**/*.php').on('change', browserSync.reload);
    gulp.watch('./js/**/*.js').on('change', browserSync.reload);
}

exports.style = style;
exports.watch = watch;