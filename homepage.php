<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<?php if(get_field('show_cinco_de_mayo_countdown', 'options') == 'on'): ?>

		<section id="cinco-de-mayo">
			<div class="wrapper">

				<div id="countdown" data-end-time="<?php the_field('cinco_de_mayo_time', 'options'); ?>" data-post-message="<?php the_field('post_cinco_de_mayo_message', 'options'); ?>">
					<h2><?php the_field('cinco_de_mayo_message', 'options'); ?> <span id="clock"></span></h2>
				</div>

			</div>
		</section>

	<?php endif; ?>

	<section id="photos">
		<div class="wrapper">

			<?php if(have_rows('photos')): ?>
				<div class="photos-wrapper">
			
					<?php while(have_rows('photos')): the_row(); ?>
			 
					    <div class="photo">
					    	<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					    </div>

					<?php endwhile; ?>

				</div>

			<?php endif; ?>

		</div>
	</section>

	<section id="info">
		<div class="wrapper">

			<div class="info-wrapper">
				<div class="photo">
					<img src="<?php $image = get_field('photo_frame'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="description">
					<?php the_field('description'); ?>
				</div>

				<div class="contact-details">
					<div class="hours details">
						<h3>Hours</h3>
						<p><?php the_field('hours', 'options'); ?></p>
					</div>

					<div class="location details">
						<h3>Location</h3>
						<p><?php the_field('location', 'options'); ?></p>
					</div>
				</div>
			</div>

		</div>
	</section>

<?php get_footer(); ?>