$(document).ready(function() {

	// Cinco de May countdown clock
	var endTime = $('#countdown').data('end-time');
	var postMessage = $('#countdown').data('post-message');

	$('#clock').countdown(endTime)
	.on('update.countdown', function(event) {
		var format = '%-D days %-H hours %-M minutes %-S seconds';

		$(this).html(event.strftime(format));
	})
	.on('finish.countdown', function(event) {
		$('#countdown h2').html(postMessage);
	});

});